
  

  

# Lumen PHP Framework

  

  

This setup use Lumen 8, Passport Authentication, and Activity Log

  

  

## Official Documentation

  

  

  

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

  

  

  

## Intallation

  

  

Execute in your terminal

  

  

> composer install

  

  

> cp .env.example .env


!! Setup your database on .env file !!


> php artisan cache:clear
  



> php artisan migrate

  

  

> php artisan passport:client --personal

  

  

> php -S 127.0.0.1:8080 -t public

  

open http://127.0.0.1:8080 to make sure it's work

  

Enjoy coding

  
## Passport Test

   

> php artisan db:seed

Login Url: http://127.0.0.1:8080/api/v1/auth/login 


Sample Account



    email: user@example.com
    password: secret




Passport Check Url: http://127.0.0.1:8080/api/v1/auth/check 

  

## Features

  

  

  

1. Make Model + migration in console

  

  

> php artisan make:model ExampleModel

  

  

2. Model with Signature column

  

  

> php artisan make:model ExampleModel --signature=true

  

  

3. Make Controller with pattern in console

  

  

> php artisan make:controller ExampleController

  

  

4. include default helper Laravel

  

  
5. Update Table with Versioning

  

  

> php artisan update:table table_names

# Created with ❤️
