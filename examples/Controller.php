<?php

namespace name_space_example;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Libraries\Response;

class Example extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            // create your logic here

            return Response::success();
        } catch (\Exception $e) {
            throw new Exception($e);
            return Response::failed(null, $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            // create your logic here

            return Response::success();
        } catch (\Exception $e) {
            throw new Exception($e);
            return Response::failed(null, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            // create your logic here

            return Response::success();
        } catch (\Exception $e) {
            throw new Exception($e);
            return Response::failed(null, $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            // create your logic here

            return Response::success();
        } catch (\Exception $e) {
            throw new Exception($e);
            return Response::failed(null, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            // create your logic here

            return Response::success();
        } catch (\Exception $e) {
            throw new Exception($e);
            return Response::failed(null, $e->getMessage());
        }
    }
}
