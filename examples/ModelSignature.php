<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Uuid;
use App\Traits\Signature;

class Example extends Model
{
    use HasFactory, SoftDeletes, Uuid;
    use Signature;

    protected $fillable = [
        'uuid',

        //add here

        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',

        'created_by',
        'updated_by',
        'deleted_by',
    ];
}
