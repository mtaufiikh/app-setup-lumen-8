<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Uuid;

class Example extends Model
{
    use HasFactory, SoftDeletes, Uuid;

    protected $fillable = [
        'uuid',
        //add here
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
