<?php

/**
 * @author mtaufiikh@gmail.com
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:model {name} {--signature=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make custom model with migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');

        if($this->option('signature')){
            $buffer = file_get_contents(base_path('examples/ModelSignature.php'));
        }else{
            $buffer = file_get_contents(base_path('examples/Model.php'));
        }

        $buffer = str_replace('Example', $name, $buffer);
        file_put_contents(base_path("app/Models/{$name}.php"), $buffer);

        if($this->option('signature')){
            $buffer = file_get_contents(base_path('examples/MigrationSignature.php'));
        }else{
            $buffer = file_get_contents(base_path('examples/Migration.php'));
        }

        $class_name = Str::pluralStudly(class_basename($name));
        $buffer = str_replace('ClassName', 'Create'.$class_name.'Table', $buffer);

        $table = Str::snake(Str::pluralStudly(class_basename($name)));
        $buffer = str_replace('table_name', $table, $buffer);

        $file_name = date('Y_m_d_His').'_create_'.$table.'_table';

        file_put_contents(base_path("database/migrations/{$file_name}.php"), $buffer);

        $this->info('Successfully create Model and Migration.');
    }
}
