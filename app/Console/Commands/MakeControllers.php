<?php

/**
 * @author mtaufiikh@gmail.com
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeControllers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:controller {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->argument('path');

        $base_path = $dir = base_path('app/Http/Controllers');

        $namespace = 'App\Http\Controllers';

        $str = explode('/', $this->argument('path'));
        if(count($str) > 1){
            $class_name = $str[count($str) - 1];
            $path = explode('/'.$class_name, $path)[0];

            $to_namespace = explode('/', $path);
            $str_namespace = '';
            for ($i=0; $i < count($to_namespace); $i++) {
                $str_namespace .= '\\'.$to_namespace[$i];
            }

            $namespace = $namespace.''.$str_namespace;

            $dir = $base_path.'/'.$path;
            if (!is_dir($dir)){
                mkdir($dir, 0777, true);
            }
        }else{
            $class_name = $path;
        }

        $buffer = file_get_contents(base_path('examples/Controller.php'));
        $buffer = str_replace('Example', $class_name, $buffer);
        $buffer = str_replace('name_space_example', $namespace, $buffer);

        file_put_contents($dir."/{$class_name}.php", $buffer);

        $this->info('Successfully create controller.');
    }
}
