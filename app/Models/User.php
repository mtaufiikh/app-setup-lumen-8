<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

use App\Traits\Uuid;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable, Uuid, SoftDeletes;

    protected $fillable = [
        'uuid',
        'name',
        'email',
        'password',
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = ['password','created_at','updated_at','deleted_at'];
}
