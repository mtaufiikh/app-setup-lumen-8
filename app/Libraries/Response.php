<?php

namespace App\Libraries;

class Response
{
    public static function success($data = null, $message = "OK", $status = 200)
    {
        try {
            if($data){
                return response([
                    "status" => $status,
                    "data" => $data,
                    "message" => $message,
                ], $status);
            }else{
                return response([
                    "status" => $status,
                    "message" => $message,
                ], $status);
            }
        } catch (\Exception $e) {
            throw new Exception($e);
            return response([
                "message" => $e->getMessage(),
            ]);
        }
    }

    public static function failed($data = null, $message = "ERROR", $status = 400)
    {
        try {
            if($data){
                return response([
                    "status" => $status,
                    "data" => $data,
                    "message" => $message,
                ], $status);
            }else{
                return response([
                    "status" => $status,
                    "message" => $message,
                ], $status);
            }
        } catch (\Exception $e) {
            throw new Exception($e);
            return response([
                "message" => $e->getMessage(),
            ]);
        }
    }

    public static function paginate($data, $message = "OK", $status = 200)
    {
        try {
            $arr = $data->toArray();

            $result = [];
            foreach ($arr['data'] as $key => $value) {
                $result[] = $value;
            }

            $arr['data'] = $result;

            $data = array_merge($arr, ['message' => $message]);
            return response($data, $status);

        } catch (\Exception $e) {
            throw new Exception($e);
            return response([
                "message" => $e->getMessage(),
            ]);
        }
    }
}
