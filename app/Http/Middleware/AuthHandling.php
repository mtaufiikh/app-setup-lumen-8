<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;

use App\Models\OauthAccessToken;

use Carbon\Carbon;

use Closure;
use Auth;

class AuthHandling
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        date_default_timezone_set("Asia/Jakarta");

        if(Auth::user()){

            activity()->withProperties([
                'method'=> request()->method(),
                'ip'    => request()->ip(),
                'data' => $request->all()
            ])
            ->log(request()->path());

            if($this->token_expired_handling()){
                return response([
                    'status' => 401,
                    'message' => 'Token Expired',
                ], 401);
            }

            if($this->verification_handling()){
                return response([
                    'status' => 406,
                    "message"=> 'Your account has not been verified.',
                ], 406);
            }

            if($this->activation_handling()){
                return response([
                    'status' => 401,
                    "message"=> 'Your account is nonactive.',
                ], 401);
            }

            return $next($request);
        }else{
            return response([
                'status' => 401,
                'message' => 'Your session is finished, please logout and re-login',
            ], 401);
        }
    }

    public function token_expired_handling()
    {
        date_default_timezone_set("Asia/Jakarta");

        $now = date('Y-m-d H:i:s');

        $token = OauthAccessToken::where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->first();

        if($token){
            if (Carbon::parse($token->expires_at) < Carbon::now()) {
                return true;
            }
        }
    }

    public function activation_handling()
    {
        if(!Auth::user()->active){
            return true;
        }
    }

    public function verification_handling()
    {
        if(!Auth::user()->verified){
            return true;
        }
    }
}
