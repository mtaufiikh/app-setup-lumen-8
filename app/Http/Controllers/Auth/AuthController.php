<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            date_default_timezone_set("Asia/Jakarta");

            $user = User::where('email', $request->email)->first();

            if(!$user){
                return response([
                    "status" => 401,
                    "message"=> 'Login failed. Account not found.',
                ], 401);
            }

            activity()->withProperties([
                'method'=> request()->method(),
                'ip'    => request()->ip(),
                'email' => $request->email,
                'password' => $request->password
            ])
            ->log(request()->path());

            if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken('app');

                    return response([
                        "status" => 200,
                        "token" => $token->accessToken,
                        "message"=> 'Login Success.',
                    ], 200);
            }else{
                return response([
                    "status" => 401,
                    "message"=> 'Login failed. Please check your password',
                ], 401);
            }
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function auth_check()
    {
        if(Auth::check()){
            return response([
                "status" => 200,
                "message"=> 'Passport OK',
            ], 200);
        }else{
            return response([
                "status" => 401,
                "message"=> 'Not Authorized',
            ], 401);
        }
    }
}
